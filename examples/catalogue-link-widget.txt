<?php if (($catalogue_link = get_option('mazzocca_catalogue_link')) && ($catalogue_url = get_option('mazzocca_current_catalogue'))) : ?>
<div id='catalogue'><a href="<?php echo $catalogue_url; ?>"><?php echo $catalogue_link; ?></a></div>
<?php endif; ?>

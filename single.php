<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package     WordPress
 * @subpackage  Starkers
 * @since       Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php $single_post_id = $post->ID; ?>
<div class='post-list'>
  <?php $categories = get_the_category();?>
  <?php $category_query = new WP_Query('category_name='.$categories[0]->name); ?>
  <?php if ( $category_query->have_posts() ): ?>
  <ol>
    <?php while ( $category_query->have_posts() ) : $category_query->the_post(); ?>
    <li>
      <?php if ($post->ID == $single_post_id) : ?>
      <div class='current-post-info'>
        <h2><?php the_title(); ?></h2>
        <?php if (in_category('artist')) : ?>
        <p id='caption'></p>
        <div id='artist-info'>
          <?php the_content(); ?>
        </div>
        <?php endif; ?>
        <div class="hr"><hr /></div>
      <?php else: ?>
      <div class='inactive-post-link'>
        <h2><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
      <?php endif; ?>
      </div>
    </li>
    <?php endwhile; ?>

    <?php get_template_part('parts/shared/catalogue_link'); ?>

  </ol>
  <?php endif; ?>
  <?php wp_reset_postdata(); ?>
</div>
<?php $main_category = (empty($categories) ? 'uncategorized' : $categories[0]->name); ?>
<div id='post-content' class='<?php echo $main_category; ?>-content'>

<?php if (in_category('artist')) : ?>

<?php $images = get_posts(array('post_parent' => $post->ID, 'post_type' => 'attachment', 'numberposts' => -1, 'post_mime_type' => 'image',));?>
<?php 
$image_srcs = array();
$image_size = get_option('mazzocca_image_size');
$max_height = 0;
foreach ($images as $image) {
  $src = wp_get_attachment_image_src($image->ID, $image_size);
  $image_srcs[] = array(
  "src" => $src,
  "id" => $image->ID,
  "desc" => apply_filters('the_description', $image->post_content));
  if ($src[2] > $max_height) {
    $max_height = $src[2];
  }
} ?>

<div id='prev-container' class='arrow'>
  <img src="<?php echo get_bloginfo('template_directory');?>/images/arrow_left.png" />
</div>

<div id="slides">

  <?php foreach ($image_srcs as $image) {
    echo '<img src="'.$image["src"][0].'"';
    echo ' alt="'.$image["desc"].'"';
    echo ' height="'.$image["src"][2].'" />';
  } ?>

</div>

<div id='next-container' class='arrow'>
  <img src="<?php echo get_bloginfo('template_directory');?>/images/arrow_right.png" />
</div>

<script>
$(function() {
  $('#slides').cycle({
    prev: '#prev-container',
    next: '#next-container',
    timeout: 0,
    sync: 0,
    speed: 500,
    after: function() {
      $('#caption').html(this.alt);
    }
  });
  // show prev and next arrows, and set their vertical position
  $('.arrow').css({
    'display': 'inline-block'
  });
});
</script>

<?php else : ?>

<div class='article' id='exhibition'>
  <?php the_content(); ?>
</div>

<?php endif; ?>
</div>
<?php endwhile; ?>
<br class='clear'>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
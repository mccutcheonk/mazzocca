
<?php if (($catalogue_link = get_option('mazzocca_catalogue_link')) && ($catalogue_url = get_option('mazzocca_current_catalogue'))) : ?>
<div id='catalogue'>
  <div class="hr"><hr /></div>
  <div id="catalogue-link"><a href="<?php echo $catalogue_url; ?>" target="_blank"><?php echo $catalogue_link; ?></a></div>
  <div class="hr"><hr /></div>
</div>
<?php endif; ?>

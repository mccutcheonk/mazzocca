<div id='header'>
<div id='logo-container'>
<a href="<?php echo home_url(); ?>">
  <?php $header_image = get_header_image();
  if ( ! empty( $header_image ) ) : ?>
  <img src="<?php echo esc_url( $header_image ); ?>" class="header-image" alt="<?php bloginfo('name'); ?>" />
<?php endif; ?>
</a>
</div>
<div id="navi">
<?php wp_nav_menu(array('menu' => 'Header Menu')); ?>
</div>
<div class="hr"><hr /></div>
</div>
<br class="clear" />

<?php
	/**
	 * Starkers functions and definitions
	 *
	 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
	 *
 	 * @package 	WordPress
 	 * @subpackage 	Starkers
 	 * @since 		Starkers 4.0
	 */

	/* ========================================================================================================================
	
	Required external files
	
	======================================================================================================================== */

	require_once( 'external/starkers-utilities.php' );

	/* ========================================================================================================================
	
	Theme specific settings

	Uncomment register_nav_menus to enable a single menu with the title of "Primary Navigation" in your theme
	
	======================================================================================================================== */

	add_theme_support('post-thumbnails');
	add_theme_support('custom-header', array(
						  'default-image'          => get_template_directory_uri() . 'images/header.jpg',
						  'uploads' => true));
    add_theme_support('menus');
	
    function register_menus() {
        register_nav_menus(array('header' => 'Header Navigation'));
    }
    add_action('init', 'register_menus');

    add_filter('widget_text','execute_php',100);
    function execute_php($html){
        if (strpos($html,"<"."?php")!==false){
            ob_start();
            eval("?".">".$html);
            $html=ob_get_contents();
            ob_end_clean();
        }
        return $html;
    }


    if ( function_exists('register_sidebar') ) {
        register_sidebar(array(
            'name', 'mazzocca-sidebar',
            'id', 'mazzocca-sidebar',
        ));
    }


function mazzocca_customize_register($wp_customize)
{
	$wp_customize->add_section('mazzocca_contact_image', array(
								   'title' => __('Contact Image', 'mazzocca'),
								   'priority' => 350,
								   ));

	$wp_customize->add_setting('mazzocca_contact_image', array(
								   'default' => '',
								   'type' => 'option',
								   'capability' => 'edit_theme_options',
								   ));

	$wp_customize->add_control(new WP_Customize_Image_Control(
								   $wp_customize,
								   'contact_image',
								   array(
									   'label' => __('Contact Image', 'mazzocca'),
									   'section' => 'mazzocca_contact_image',
									   'settings' => 'mazzocca_contact_image',
									   'uploads' => true,
									   )));

	$wp_customize->add_section('mazzocca_catalogue', array(
								   'title' => __('Catalogue', 'mazzocca'),
								   'priority' => 360,
								   ));

	$wp_customize->add_setting('mazzocca_current_catalogue', array(
								   'default' => '',
								   'type' => 'option',
								   'capability' => 'edit_theme_options',
								   ));

	$wp_customize->add_control(new WP_Customize_Upload_Control(
								   $wp_customize,
								   'current_catalogue',
								   array(
									   'label' => __('Current Catalogue', 'mazzocca'),
									   'section' => 'mazzocca_catalogue',
									   'settings' => 'mazzocca_current_catalogue',
									   'uploads' => true,
									   )));

	$wp_customize->add_setting('mazzocca_catalogue_link', array(
								   'default' => '',
								   'type' => 'option',
								   'capability' => 'edit_theme_options',
								   ));

	$wp_customize->add_control('catalogue_link',
							   array(
								   'label' => __('Catalogue Link', 'mazzocca'),
								   'section' => 'mazzocca_catalogue',
								   'settings' => 'mazzocca_catalogue_link',
								   ));

	$wp_customize->add_section('mazzocca_image_display', array(
								   'title' => __('Image Display', 'mazzocca'),
								   'priority' => 370,
								   ));

	$wp_customize->add_setting('mazzocca_image_size', array(
								   'default' => '',
								   'type' => 'option',
								   'capability' => 'edit_theme_options',
								   ));

	$wp_customize->add_control('image_size',
							   array(
								   'label' => __('Image Size', 'mazzocca'),
								   'section' => 'mazzocca_image_display',
								   'settings' => 'mazzocca_image_size',
								   'type' => 'select',
								   'choices' => array(
									   'medium' => 'medium',
									   'large' => 'large',
									   'full' => 'full',
									   ),
								   ));

}
add_action('customize_register', 'mazzocca_customize_register');
	/* ========================================================================================================================
	
	Actions and Filters
	
	======================================================================================================================== */

	add_action( 'wp_enqueue_scripts', 'starkers_script_enqueuer' );

	add_filter( 'body_class', array( 'Starkers_Utilities', 'add_slug_to_body_class' ) );

	/* ========================================================================================================================
	
	Custom Post Types - include custom post types and taxonimies here e.g.

	e.g. require_once( 'custom-post-types/your-custom-post-type.php' );
	
	======================================================================================================================== */



	/* ========================================================================================================================
	
	Scripts
	
	======================================================================================================================== */

	/**
	 * Add scripts via wp_head()
	 *
	 * @return void
	 * @author Keir Whitaker
	 */

	function starkers_script_enqueuer() {
		if( !is_admin() ) {
			wp_deregister_script('jquery');
			wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false, 'latest', false);
			wp_enqueue_script('jquery');
		}

		wp_register_style( 'screen', get_stylesheet_directory_uri().'/style.css', '', '', 'screen' );
        wp_enqueue_style( 'screen' );
 
		wp_register_script( 'cycle', get_template_directory_uri().'/js/jquery.cycle.lite.js', array( 'jquery' ) );
		wp_enqueue_script( 'cycle' );
		
		wp_register_script( 'site', get_template_directory_uri().'/js/site.js', array( 'jquery' ) );
		wp_enqueue_script( 'site' );

	}	

	/* ========================================================================================================================
	
	Comments
	
	======================================================================================================================== */

	/**
	 * Custom callback for outputting comments 
	 *
	 * @return void
	 * @author Keir Whitaker
	 */
	function starkers_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment; 
		?>
		<?php if ( $comment->comment_approved == '1' ): ?>	
		<li>
			<article id="comment-<?php comment_ID() ?>">
				<?php echo get_avatar( $comment ); ?>
				<h4><?php comment_author_link() ?></h4>
				<time><a href="#comment-<?php comment_ID() ?>" pubdate><?php comment_date() ?> at <?php comment_time() ?></a></time>
				<?php comment_text() ?>
			</article>
		<?php endif;
	}
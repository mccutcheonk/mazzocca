<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file 
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package     WordPress
 * @subpackage  Starkers
 * @since       Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
<div class='post-list'>
  <?php $artists_query = new WP_Query('category_name=artist'); ?>
  <?php if ( $artists_query->have_posts() ): ?>
  <ol>
    <?php while ( $artists_query->have_posts() ) : $artists_query->the_post(); ?>
    <li>
      <div>
        <h2><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
      </div>
    </li>
    <?php endwhile; ?>

    <?php get_template_part('parts/shared/catalogue_link'); ?>

  </ol>
  <?php endif; ?>
</div>

<br class='clear'>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer') ); ?>


	MAZZOCCA

	Created by Killian McCutcheon

	- - - - - - - - - - - - - - - - - - - - - - -

	Mazzocca is a WordPress theme created for the
	Mazzocca Contemporary Paintings website, based
	on the bare-bones Starkers theme by Viewpoint
	Industries Ltd.

	Mazzocca is not intended to be a general-purpose
	theme - its functionality is tailored to the
	needs of the gallery website.

	For more information on the Starkers theme,
	please visit:
	http://viewportindustries.com/products/starkers/

